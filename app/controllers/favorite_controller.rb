class FavoriteController < ApplicationController
  before_action :find_restaurant
  before_action :authenticate_user!
  
  def create
    @favorite = @restaurant.favorites.build(favorite_params)
    @favorite.user_id = current_user.id 
    @favorite.save
    render :index
  end
  
  private
  
  def find_restaurant
    @restaurant = Restaurant.find(params[:restaurant_id])
  end
  
  def favorite_params 
      params.require(:favorite).permit(:fave, :user_id, :restaurant_id)    
  end
  
end
