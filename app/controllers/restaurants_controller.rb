class RestaurantsController < ApplicationController
  before_action :set_restaurant, except: [:index, :new, :create, :next, :previous, :user_history]
  before_action :set_users
  before_action :set_votes, except: [:user_history]
  before_action :set_user_votes, only: [:user_history]
  before_action :set_comments, except: [:user_history]
  before_action :set_user_comments, only: [:user_history]
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_favorites, only: [:user_history]

  # GET /restaurants
  # GET /restaurants.json
  def index
    session[:page] = 0
    @restaurants = Restaurant.limit(10).offset(session[:page] * 10) 
    if params[:search]
      @restaurants = Restaurant.search(params[:search])
    else
      @restaurants = Restaurant.all
    end
  end

  def history
     render :history
  end
  
  def favorite
    @favorite = @restaurant.favorites.build(:fave => "true")
    @favorite.user_id = current_user.id 
    @favorite.restaurant_id = params[:id]
    @favorite.save
    redirect_back fallback_location: root_url  
  end
  

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
    @comments = Comment.where(restaurant_id: @restaurant).order("created_at DESC")
  end

  # GET /restaurants/new
  def new
    @restaurant = current_user.restaurants.build
  end
  
  def will
    @restaurant.vote_by voter: current_user, :vote => 'like', :duplicate => true
    redirect_back fallback_location: root_url  
  end
  
  def wont
    @restaurant.vote_by voter: current_user, :vote => 'bad', :duplicate => true
    redirect_back fallback_location: root_url 
  end
  
  def next
    if (session[:page] == 0) 
      @restaurants = Restaurant.limit(10).offset(session[:page] * 10)      
    else
      session[:page] += 1
      @restaurants = Restaurant.limit(10).offset(session[:page] * 10)   
    end
    render :index     
  end

  def previous
    if (session[:page] == 0) 
      @restaurants = Restaurant.limit(10).offset(session[:page] * 10)      
    else
      session[:page] -= 1
      @restaurants = Restaurant.limit(10).offset(session[:page] * 10)   
    end
    render :index    
  end
  

  # POST /restaurants
  # POST /restaurants.json
  def create
    @restaurant = current_user.restaurants.build(restaurant_params)

    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully created.' }
        format.json { render :show, status: :created, location: @restaurant }
      else
        format.html { render :new }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def restaurant_params
      params.require(:restaurant).permit(:name, :street, :city, :state)
    end
    
    def set_votes
      @votes = ActsAsVotable::Vote.all      
    end
    
    def set_users
      @users = User.all
    end
    
    def set_comments
      @comments = Comment.all
    end
    
    def set_user_comments
      @comments = Comment.select { |comment| comment.user_id == current_user.id }    
    end
    
    def set_user_votes
      @votes = ActsAsVotable::Vote.select { |vote| vote.voter_id == current_user.id }     
    end
    
    def set_favorites
      @favorites = Favorite.select {|favorite| favorite.user_id == current_user.id }
    end
    
end
