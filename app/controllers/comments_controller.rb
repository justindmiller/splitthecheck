class CommentsController < ApplicationController
  before_action :find_restaurant
  before_action :authenticate_user!
  
  def create
    @comment = @restaurant.comments.build(comment_params)
    @comment.user_id = current_user.id 
    @comment.save
    
    if @comment.save
      redirect_to restaurant_path(@restaurant) 
    else
      render 'new'
    end
  end
  
  private
  
  def find_restaurant
    @restaurant = Restaurant.find(params[:restaurant_id])
  end
  
  def comment_params 
      params.require(:comment).permit(:content)    
  end
  
end
