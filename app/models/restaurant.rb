class Restaurant < ApplicationRecord
  
  belongs_to :user
  acts_as_votable  
  default_scope -> {order(created_at: :desc)}
  validates :name, :street, :city, :state, presence: true
  
  def self.search(search)
    where("name LIKE ? OR state LIKE ? OR street LIKE ? OR city LIKE ?", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%")
  end
  
  has_many :comments
  has_many :favorites
end
