Rails.application.routes.draw do
  devise_for :users
  root 'restaurants#index'
  
  get '/restaurants/index', 'restaurants#index'
  get '/restaurants/oldest', 'restaurants#oldest'
  get '/restaurants/next', 'restaurants#next'
  get '/restaurants/previous', 'restaurants#previous'
  get '/restaurants/user_history', 'restaurants#user_history'

  
  resources :restaurants, :except => [:destroy] do
    resources :comments
    member do 
      put "will", to: 'restaurants#will'
      put "wont", to: 'restaurants#wont'
      put "favorite", to: 'restaurants#favorite'
    end
  end  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
