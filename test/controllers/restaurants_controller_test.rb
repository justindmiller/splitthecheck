require 'test_helper'

class RestaurantsControllerTest < ActionDispatch::IntegrationTest
  
  include Devise::Test::IntegrationHelpers
  
  setup do
    @restaurants = Restaurant.all
    @restaurant = restaurants(:one)
  end

  test "should get index" do 
    sign_in users(:one)
    get restaurants_url
    assert_response :success
  end

  test "should get new" do
    sign_in users(:one)
    get new_restaurant_url
    assert_response :success
  end

  test "should create restaurant" do
    sign_in users(:one)
    post restaurants_url, params: { restaurant: { city: @restaurant.city, name: @restaurant.name, state: @restaurant.state, street: @restaurant.street } }
    assert_equal(3, Restaurant.count)
  end

  test "should show restaurant" do
    
    get restaurant_url(@restaurant)
    assert_response :success
  end

  test "should go forward then back to newest" do
    get restaurants_url
    get restaurants_next_path
    get restaurants_path
    assert_equal(0, session[:page])
  end
  
  test "should go forward then back" do
    get restaurants_url
    get restaurants_next_path
    get restaurants_previous_path
    assert_equal(0, session[:page])
  end  
  
  test "default votes should be 0" do
    assert_equal(0, @restaurants.last.get_upvotes.size)
  end
  
  test "should add 1 to will split" do
    
    sign_in users(:one)
    put will_restaurant_path(@restaurants.last)
    assert_equal(1, @restaurants.last.get_upvotes.size) 
  end
  
  test "should add 1 to wont split" do
    sign_in users(:one)
    put wont_restaurant_path(@restaurants.last)
    assert_equal(1, @restaurants.last.get_downvotes.size)
  end
  
  test "should get user history" do
    sign_in users(:three)
    get restaurants_user_history_path
    assert_response :success
  end
  
  test "should add favorite" do
    sign_in users(:one)
    put favorite_restaurant_path(@restaurant)
    assert_equal(3, Favorite.count)
  end

end
