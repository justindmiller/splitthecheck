require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest

  include Devise::Test::IntegrationHelpers

  setup do
    @comments = Comment.all
    @comment = comments(:one)
  end

  test "should create comment" do
    sign_in users(:one)
    @restaurant = restaurants(:one)
    post restaurant_comments_path(@restaurant), params: { comment: { content: @comment.content, restaurant: @comment.restaurant, user: @comment.user } }
    assert_equal(3, Comment.count)
  end
  
end
